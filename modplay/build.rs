use std::env;

fn main() {
    let dkp_path = env::var("DEVKITPRO").unwrap();
    match env::var("TARGET").unwrap().as_str() {
        "powerpc-dolphin-eabi" => println!(
            "cargo:rustc-link-search=native={}/libogc/lib/cube",
            dkp_path
        ),
        "powerpc-revolution-eabi" => {
            println!("cargo:rustc-link-search=native={}/libogc/lib/wii", dkp_path)
        }
        _ => println!("cargo:warning=\"Building on unsupported system. YMMV\""),
    }
    println!("cargo:rustc-link-lib=static=modplay");
}
